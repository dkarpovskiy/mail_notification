from email.mime.application import MIMEApplication
from email.mime.base import MIMEBase
from email import encoders
from os import path
import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.utils import formataddr
from email.header import Header
import argparse
from email.mime.text import MIMEText
from os import listdir
from os.path import isfile, join, basename
import socks


MAIL_CONFIG_PATH = "mail.json"

def send_mail(to_address, subject, message_body_path, attach_path):
    mail_config = generate_mail_config()
    message = generate_message(to_address, mail_config, subject, message_body_path, attach_path)
    server = smtplib.SMTP_SSL(mail_config["address"])
    server.set_debuglevel(2)
    server.ehlo(mail_config["user"])
    server.login(mail_config["user"], mail_config["password"])
    server.auth_plain()
    server.sendmail(mail_config["user"], to_address, message.as_string())
    server.quit()


def generate_message(to_address, mail_config, subject, message_body_path, attach_path):
    message = MIMEMultipart("mixed")
    message["From"] = formataddr((str(Header(mail_config["replacement_address"], 'utf-8')), mail_config["user"]))
    message["To"] = to_address
    message["Subject"] = subject
    message.attach(MIMEText(open(message_body_path).read(), 'html'))
    if attach_path:
        attach_files(attach_path, message)
    return message



def attach_files(path, message):
    files = [join(path, f) for f in listdir(path) if isfile(join(path, f))]
    for file in files:
        with open(file, "rb") as f:
            part = MIMEApplication(f.read(), name=basename(file))
        part['Content-Disposition'] = f'attachment; filename="{basename(file)}"'
        message.attach(part)


def generate_mail_config():
    with open(f"{path.dirname(path.abspath(__file__))}/{MAIL_CONFIG_PATH}", 'r') as configuration_file:
        return json.load(configuration_file)["mail"]


def parse_args():
    parser = argparse.ArgumentParser(description='повторение английских слов')
    parser.add_argument('--to-address', help='Адрес, кому нужно отправить сообщение', required=True)
    parser.add_argument('--subject', help='Тема письма', required=True)
    parser.add_argument('--message-body-path', help='Путь до тела письма', required=True)
    parser.add_argument('--proxy-config-path', help='Путь до конфигурации прокси')
    parser.add_argument('--attach-path', help='Путь до директории, из которой нужно прикрепить фалы к письму')
    return parser.parse_args()


def main():
    args = parse_args()
    if args.proxy_config_path:
        with open(args.proxy_config_path, 'r') as config:
            config = json.load(config)["proxy"]
            socks.setdefaultproxy(config["http/https"], config["host"], config["port"])
            socks.wrapmodule(smtplib)
    send_mail(args.to_address, args.subject, args.message_body_path, args.attach_path)


if __name__ == '__main__':
    main()
